package ethens.hike

import android.app.Application
import com.facebook.stetho.Stetho

class HikeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}