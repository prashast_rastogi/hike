package ethens.hike.adapter

import androidx.recyclerview.widget.DiffUtil
import ethens.hike.data.Photo

class ImageListDiffCallback  : DiffUtil.ItemCallback<Any>(){
    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
        if(oldItem is Photo && newItem is Photo){
            return oldItem.id == newItem.id
        }
        return false
    }

    override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
        if(oldItem is Photo && newItem is Photo){
            return oldItem.getUrl() == newItem.getUrl()
        }
        return false
    }

}