package ethens.hike.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import ethens.hike.data.Photo

class ImageListAdapter : ListAdapter<Any, ImageListViewHolder>(ImageListDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ImageListViewHolder.create(inflater, parent)
    }

    override fun onBindViewHolder(holder: ImageListViewHolder, position: Int) {
        holder.bind(getItem(position) as Photo)
    }

}