package ethens.hike.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ethens.hike.R
import ethens.hike.data.Photo
import ethens.hike.setImage

class ImageListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    companion object {

        fun create(
            inflater: LayoutInflater,
            viewGroup: ViewGroup
        ): ImageListViewHolder {
            val view = inflater.inflate(R.layout.image_item, viewGroup, false)
            return ImageListViewHolder(view)
        }
    }

    fun bind(image: Photo) {
        itemView.findViewById<ImageView>(R.id.photo_iv).setImage(image.getUrl())
        itemView.findViewById<TextView>(R.id.photo_name).text = image.title
    }
}