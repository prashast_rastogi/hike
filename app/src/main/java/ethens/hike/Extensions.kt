package ethens.hike

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

fun AppCompatActivity.addFragment(id: Int, fragment: Fragment, tag: String) {
    supportFragmentManager.transact {
        add(id, fragment)
    }
}

private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

fun RecyclerView.addOnScrollListener(scrollDown: () -> Unit, scrollUp: () -> Unit) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy > 0)
                scrollDown()
            else
                scrollUp()
        }
    })
}

fun ImageView.setImage(imageUrl: String) {
    Glide.with(context)
        .load(imageUrl)
        .apply(getRequestOptions(context))
        .into(this)
}

private fun getRequestOptions(context: Context, placeholder: Int? = null): RequestOptions {
    val requestOptions = RequestOptions()
    if (placeholder != null) requestOptions.placeholder(placeholder)
    else requestOptions.placeholder(
        ColorDrawable(
            ContextCompat.getColor(
                context, R.color.placeholder_gray
            )
        )
    )

    return requestOptions
}

fun LinearLayoutManager.hitBottomOfList(): Boolean {
    val visibleItemCount: Int = childCount
    val totalItemCount: Int = itemCount
    val pastVisibleItems: Int = findFirstVisibleItemPosition()
    return pastVisibleItems + visibleItemCount >= totalItemCount - 20
}

object RxSearchObservable {
    fun fromChangeView(searchView: android.widget.SearchView): Observable<String> {
        val subject = PublishSubject.create<String>()
        searchView.setOnQueryTextListener(object :
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { subject.onNext(it) }
                return false
            }

        })
        return subject
    }
}

operator fun CompositeDisposable?.plus(disposable: Disposable?): CompositeDisposable? {
    disposable?.let {
        this?.add(it)
    }
    return this
}