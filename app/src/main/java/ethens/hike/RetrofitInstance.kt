package ethens.hike

import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    private lateinit var instance: Retrofit
    private lateinit var okHttpClient: OkHttpClient
    private const val BASE_URL = "https://api.flickr.com/"


    @Synchronized
    private fun createInstance(baseUrl: String): Retrofit {

        if (!::okHttpClient.isInitialized) {
            okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()
        }

        return Retrofit.Builder()
            .baseUrl("$baseUrl/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Synchronized
    private fun createInstance() {
        instance = createInstance(BASE_URL)
    }


    fun getInstance(): Retrofit {
        if (!::instance.isInitialized) createInstance()
        return instance
    }

}