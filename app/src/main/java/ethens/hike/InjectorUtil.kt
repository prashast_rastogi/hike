package ethens.hike

import ethens.hike.viewmodel.ImageListViewModelFactory

object InjectorUtil {

    private fun getImageListRepo(): ImageListRepo {
        return ImageListRepo()
    }

    fun provideImageListViewModelFactory() : ImageListViewModelFactory{
        return ImageListViewModelFactory(getImageListRepo())
    }


}