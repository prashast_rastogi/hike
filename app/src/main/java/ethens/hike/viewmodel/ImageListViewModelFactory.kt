package ethens.hike.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ethens.hike.ImageListRepo

class ImageListViewModelFactory(
    private val repo : ImageListRepo
): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ImageListViewModel(repo) as T
    }
}