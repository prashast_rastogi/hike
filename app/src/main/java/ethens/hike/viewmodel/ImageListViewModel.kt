package ethens.hike.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ethens.hike.ImageListRepo
import ethens.hike.RequestResult
import ethens.hike.data.ImageSearchResponse
import ethens.hike.data.Photo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ImageListViewModel(private val repo: ImageListRepo) : ViewModel() {

    val getImageList = MutableLiveData<RequestResult<Any?>>()

    private var page = 0
    private var query : String = ""

    private val disposer  = ArrayList<Disposable>()

    fun getImageList(query: String, page : Int) {
        disposer + repo.getImageList(query, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onGetImageSearchResponse(it, page, query)
            }, {

            })
    }

    fun getImageList(){
        getImageList(query, page)
    }

    private fun onGetImageSearchResponse(
        imageResponse: ImageSearchResponse?,
        page: Int,
        query: String
    ) {
        if(page == 0){
            this.page = page + 1
            this.query = query
            getImageList.value = RequestResult.Success(imageResponse?.photos?.photo)
        }else{
            val prevResponse = getImageList.value as RequestResult.Success
            prevResponse.data?.let {
                val imageList = it as ArrayList<Photo>
                imageResponse?.photos?.photo?.let { imageList.addAll(it) }
                getImageList.value = RequestResult.Success(imageList)
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        disposer.map {
            it.dispose()
        }
    }
}