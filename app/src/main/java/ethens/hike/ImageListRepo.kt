package ethens.hike

import ethens.hike.data.ImageSearchResponse
import io.reactivex.Single

class ImageListRepo : BaseRepo(){
    private val service = retrofit.create(ImageApiService::class.java)

    fun getImageList(query : String, page : Int) : Single<ImageSearchResponse> {
        return service.getQueryResult(query = query, page = page)
    }

}