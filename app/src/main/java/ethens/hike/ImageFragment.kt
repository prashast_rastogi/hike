package ethens.hike

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ethens.hike.adapter.ImageListAdapter
import ethens.hike.viewmodel.ImageListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.image_list_fragment.*
import kotlinx.android.synthetic.main.search_toolbar.*
import java.util.concurrent.TimeUnit

class ImageFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.image_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        initSearchView()
        initSearchMechanism()
        initAdapter()
        initListeners()
        initViewModelObservers()
        viewModel.getImageList()
    }

    private fun initViewModelObservers() {
        viewModel.getImageList.observe(viewLifecycleOwner, Observer {
            onGetImageListResponse(it as RequestResult<*>)
        })
    }

    private fun onGetImageListResponse(requestResult: RequestResult<Any?>) {
        when (requestResult) {
            is RequestResult.Success -> onGetImageListSuccess(requestResult)
        }
    }

    private fun onGetImageListSuccess(requestResult: RequestResult.Success<Any?>) {
        requestResult.data?.let {
            loading = false
            adapter?.submitList(it as List<*>)
        }
    }

    private fun initListeners() {
        image_rv.addOnScrollListener({
            onScrolledDown()
        }, {})
    }

    private var loading = false
    private fun onScrolledDown() {
        layoutManager?.let {
            if (it.hitBottomOfList() && !loading) {
                loading = true
                viewModel.getImageList()
            }
        }
    }

    private val viewModel: ImageListViewModel by viewModels {
        InjectorUtil.provideImageListViewModelFactory()
    }

    private fun initSearchView() {
        val id: Int =
            search_view.context.resources.getIdentifier("android:id/search_src_text", null, null)
        val editText = search_view.findViewById(id) as EditText
        editText.setTextColor(requireContext().resources?.getColor(R.color.black)!!)
        editText.setHintTextColor(context?.resources?.getColor(R.color.grey)!!)

        search_view.setOnClickListener { search_view.isIconified = false }

        search_view.queryHint = "Search for Images"

        val identifier =
            search_view.context.resources.getIdentifier("android:id/search_close_btn", null, null);
        val crossIv = search_view.findViewById(identifier) as ImageView
        crossIv.setOnClickListener {
            search_view.setQuery("", false)
        }

        search_view.isIconified = false

    }

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun initSearchMechanism() {
        compositeDisposable + RxSearchObservable.fromChangeView(search_view)
            .debounce(300, TimeUnit.MILLISECONDS)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe {
                if (it.isNotEmpty() && it.length > 1)
                    viewModel.getImageList(it, 0)
            }
    }


    private var layoutManager: GridLayoutManager? = null
    private var adapter: ImageListAdapter? = null
    private fun initAdapter() {
            layoutManager = GridLayoutManager(activity,3)
            image_rv.layoutManager = layoutManager
            adapter = ImageListAdapter()
            image_rv.adapter = adapter
    }
}