package ethens.hike

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import ethens.hike.viewmodel.ImageListViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        addFragment(
            R.id.container,
            ImageFragment(),
            "ImageFragment"
        )
    }


}
