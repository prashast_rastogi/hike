package ethens.hike

import ethens.hike.data.ImageSearchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ImageApiService{

    @GET("/services/rest")
    fun getQueryResult(
        @Query("method") method: String = "flickr.photos.search",
        @Query("api_key") apiKey: String = "3e7cc266ae2b0e0d78e279ce8e361736",
        @Query("format") format: String = "json",
        @Query("nojsoncallback") noJSONFormat: Int = 1,
        @Query("text") query: String,
        @Query("page") page: Int
    ): Single<ImageSearchResponse>
}