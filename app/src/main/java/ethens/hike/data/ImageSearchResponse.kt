package ethens.hike.data

import androidx.annotation.Keep

@Keep
data class ImageSearchResponse(
    val photos: Photos
)

@Keep
data class Photos(
    val page: Int,
    val pages: Int,
    val perpage: Int,
    val total: Int,
    val photo: ArrayList<Photo>
)

@Keep
data class Photo(
    val id: String,
    val owner: String,
    val secret: String,
    val server: String,
    val title: String,
    val farm: Int,
    val ispublic: Int,
    val isfriend: Int,
    val isfamily: Int
) {
    fun getUrl(): String {
        return "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + ".jpg"
    }
}
